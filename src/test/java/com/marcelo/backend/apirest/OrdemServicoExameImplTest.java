package com.marcelo.backend.apirest;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import com.marcelo.backend.apirest.controller.OrdemServicoController;
import com.marcelo.backend.apirest.entity.Exame;
import com.marcelo.backend.apirest.entity.Medico;
import com.marcelo.backend.apirest.entity.OrdemServico;
import com.marcelo.backend.apirest.entity.OrdemServicoExame;
import com.marcelo.backend.apirest.entity.Paciente;
import com.marcelo.backend.apirest.models.dao.IOrdemServicoExameDao;
/**
 * Clase de tests unitarios - nao estou cubrindo todos os testes, mesmo porque o metodo é bem simples. É mais pra ver se eu conseguia fazer alguma test.
 * @author marce
 *
 */
@SpringBootTest
@RunWith(BlockJUnit4ClassRunner.class)
class OrdemServicoExameImplTest {

	IOrdemServicoExameDao ordemServicoExameDao = Mockito.mock(IOrdemServicoExameDao.class);

	com.marcelo.backend.apirest.models.services.impl.OrdemServicoExameServiceImpl OrdemServicoExameServiceImpl = Mockito
			.mock(com.marcelo.backend.apirest.models.services.impl.OrdemServicoExameServiceImpl.class);;

	@Autowired
	OrdemServicoController ordemServicoController;

	
	@BeforeEach
	void setUp() throws Exception {

		List<OrdemServicoExame> list = new ArrayList<OrdemServicoExame>();
		OrdemServicoExame ordemServicoExame = new OrdemServicoExame();
		ordemServicoExame.setId(1L);
		
		Exame exame = new Exame();
		exame.setId(1L);
		exame.setDescricao("Hemograma");
		exame.setPreco(300D);
		ordemServicoExame.setExame(exame);
		
		OrdemServico ordemServico = new OrdemServico();
		ordemServico.setId(1L);
		ordemServico.setConvenio("Unimed");
		Medico medico = new Medico();
		medico.setEspecialidade("Cardiologia");
		medico.setNome("Paulo Fernando");
		medico.setId(1L);
		ordemServico.setMedico(medico);
		Paciente paciente = new Paciente();
		paciente.setNome("Beatriz Pagani");
		
		ordemServico.setPaciente(paciente);
		
		ordemServicoExame.setOrdemServico(ordemServico);
		ordemServicoExame.setPreco(200D);
		
		list.add(ordemServicoExame);

		Mockito.when(OrdemServicoExameServiceImpl.findAll()).thenReturn(list);

		
		Assertions.assertEquals(1, list.get(0).getId());
		Assertions.assertEquals(1, ordemServico.getId());
		Assertions.assertEquals("Unimed", list.get(0).getOrdemServico().getConvenio());
		Assertions.assertEquals("Paulo Fernando", list.get(0).getOrdemServico().getMedico().getNome());
		Assertions.assertEquals("Beatriz Pagani", list.get(0).getOrdemServico().getPaciente().getNome());

	}

	@Test
	void testFindAll() {
		List<OrdemServicoExame> list = ordemServicoController.getAll();
	}

}
