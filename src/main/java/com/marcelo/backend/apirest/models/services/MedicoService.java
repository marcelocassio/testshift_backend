package com.marcelo.backend.apirest.models.services;

import java.util.List;
import com.marcelo.backend.apirest.entity.Medico;

public interface MedicoService {

	public List<Medico> findAll();

}
