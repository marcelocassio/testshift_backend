package com.marcelo.backend.apirest.models.services;

import java.util.List;
import com.marcelo.backend.apirest.entity.OrdemServicoExame;

public interface OrdemServicoExameService {
	
	public List<OrdemServicoExame> findAll();
	
	public OrdemServicoExame findById(Long id);
	
	public OrdemServicoExame save(OrdemServicoExame ordem);
	
	public void delete(Long id);
	
	

}
