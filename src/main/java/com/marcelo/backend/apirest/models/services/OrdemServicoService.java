package com.marcelo.backend.apirest.models.services;

import java.util.List;
import com.marcelo.backend.apirest.entity.OrdemServico;

public interface OrdemServicoService {

	public List<OrdemServico> findAll();
}
