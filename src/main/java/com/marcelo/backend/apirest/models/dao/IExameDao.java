package com.marcelo.backend.apirest.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.marcelo.backend.apirest.entity.Exame;

public interface IExameDao extends JpaRepository<Exame, Long>{

}
