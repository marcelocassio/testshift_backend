package com.marcelo.backend.apirest.models.services.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.marcelo.backend.apirest.entity.Exame;
import com.marcelo.backend.apirest.entity.Medico;
import com.marcelo.backend.apirest.entity.OrdemServicoExame;
import com.marcelo.backend.apirest.entity.PostoColeta;
import com.marcelo.backend.apirest.models.dao.IOrdemServicoExameDao;
import com.marcelo.backend.apirest.models.services.ExameService;
import com.marcelo.backend.apirest.models.services.MedicoService;
import com.marcelo.backend.apirest.models.services.OrdemServicoExameService;
import com.marcelo.backend.apirest.models.services.PostoColetaService;

@Service
public class OrdemServicoExameServiceImpl implements OrdemServicoExameService {

	@Autowired
	IOrdemServicoExameDao ordemServicoExameDao;

	@Autowired
	ExameService exameService;

	@Autowired
	MedicoService medicoService;

	@Autowired
	PostoColetaService postoService;

	@Override
	public List<OrdemServicoExame> findAll() {
		return ordemServicoExameDao.findAll();
	}

	@Override
	public OrdemServicoExame save(OrdemServicoExame ordem) {

		List<Exame> lstExame = exameService.findAll();
		List<Medico> lstMedicos = medicoService.findAll();
		List<PostoColeta> lstPostos = postoService.findAll();

		lstExame.forEach(e -> {
			if (e.getDescricao().equals(ordem.getExame().getDescricao())) {
				ordem.setPreco(e.getPreco());
				ordem.getExame().setPreco(e.getPreco());
				ordem.getExame().setId(e.getId());
			}
		});

		lstMedicos.forEach(m -> {
			if (m.getNome().equals(ordem.getOrdemServico().getMedico().getNome())) {
				ordem.getOrdemServico().getMedico().setEspecialidade(m.getEspecialidade());
				ordem.getOrdemServico().getMedico().setId(m.getId());
			}
		});

		lstPostos.forEach(p -> {
			if (p.getDescricao().equals(ordem.getOrdemServico().getPostoColeta().getDescricao())) {
				ordem.getOrdemServico().getPostoColeta().setEndereco(p.getEndereco());
				ordem.getOrdemServico().getPostoColeta().setId(p.getId());
			}
		});

		return ordemServicoExameDao.save(ordem);
	}

	@Override
	public OrdemServicoExame findById(Long id) {
		return ordemServicoExameDao.findById(id).orElse(null);
	}

	@Override
	public void delete(Long id) {
		ordemServicoExameDao.deleteById(id);
	}

}
