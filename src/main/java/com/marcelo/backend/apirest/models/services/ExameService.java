package com.marcelo.backend.apirest.models.services;

import java.util.List;
import com.marcelo.backend.apirest.entity.Exame;

public interface ExameService {

	public List<Exame> findAll();
}
