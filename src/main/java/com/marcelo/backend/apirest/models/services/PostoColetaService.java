package com.marcelo.backend.apirest.models.services;

import java.util.List;
import com.marcelo.backend.apirest.entity.PostoColeta;

public interface PostoColetaService {

	public List<PostoColeta> findAll();
}
