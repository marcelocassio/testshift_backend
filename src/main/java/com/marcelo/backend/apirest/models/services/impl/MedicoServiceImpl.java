package com.marcelo.backend.apirest.models.services.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.marcelo.backend.apirest.entity.Medico;
import com.marcelo.backend.apirest.models.dao.IMedicoDao;
import com.marcelo.backend.apirest.models.services.MedicoService;

@Service
public class MedicoServiceImpl implements MedicoService {

	@Autowired
	IMedicoDao medicoDao;

	@Override
	public List<Medico> findAll() {
		return medicoDao.findAll();
	}

}
