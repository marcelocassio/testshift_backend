package com.marcelo.backend.apirest.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.marcelo.backend.apirest.entity.OrdemServico;

public interface IOrdemServicoDao extends JpaRepository<OrdemServico, Long>{

}
