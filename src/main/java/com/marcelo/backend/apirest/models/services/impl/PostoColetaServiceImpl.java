package com.marcelo.backend.apirest.models.services.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.marcelo.backend.apirest.entity.PostoColeta;
import com.marcelo.backend.apirest.models.dao.IPostoColetaDao;
import com.marcelo.backend.apirest.models.services.PostoColetaService;

@Service
public class PostoColetaServiceImpl implements PostoColetaService {

	@Autowired
	IPostoColetaDao postoColetaDao;

	@Override
	public List<PostoColeta> findAll() {
		return postoColetaDao.findAll();
	}

}
