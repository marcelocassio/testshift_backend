package com.marcelo.backend.apirest.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.marcelo.backend.apirest.entity.OrdemServicoExame;

public interface IOrdemServicoExameDao extends JpaRepository<OrdemServicoExame, Long> {

}
