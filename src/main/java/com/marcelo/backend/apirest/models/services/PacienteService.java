package com.marcelo.backend.apirest.models.services;

import java.util.List;

import com.marcelo.backend.apirest.entity.Paciente;

public interface PacienteService {

	public List<Paciente> findAll();
	
	public Paciente findById(Long id);
}
