package com.marcelo.backend.apirest.models.services.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.marcelo.backend.apirest.entity.Paciente;
import com.marcelo.backend.apirest.models.dao.IPacienteDao;
import com.marcelo.backend.apirest.models.services.PacienteService;

@Service
public class PacienteServiceImpl implements PacienteService {

	@Autowired
	IPacienteDao pacienteDao;

	@Override
	@Transactional(readOnly = true)
	public List<Paciente> findAll() {
		return pacienteDao.findAll();
	}

	@Override
	public Paciente findById(Long id) {
		return pacienteDao.findById(id).orElse(null);
	}

}
