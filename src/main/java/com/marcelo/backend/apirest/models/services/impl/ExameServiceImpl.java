package com.marcelo.backend.apirest.models.services.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.marcelo.backend.apirest.entity.Exame;
import com.marcelo.backend.apirest.models.dao.IExameDao;
import com.marcelo.backend.apirest.models.services.ExameService;

@Service
public class ExameServiceImpl implements ExameService {

	@Autowired
	IExameDao exameDao;

	@Override
	@Transactional(readOnly = true)
	public List<Exame> findAll() {
		return exameDao.findAll();
	}

}
