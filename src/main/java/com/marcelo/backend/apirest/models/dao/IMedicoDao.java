package com.marcelo.backend.apirest.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.marcelo.backend.apirest.entity.Medico;

public interface IMedicoDao extends JpaRepository<Medico, Long> {

}
