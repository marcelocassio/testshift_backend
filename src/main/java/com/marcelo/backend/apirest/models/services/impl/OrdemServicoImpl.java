package com.marcelo.backend.apirest.models.services.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.marcelo.backend.apirest.entity.OrdemServico;
import com.marcelo.backend.apirest.models.dao.IOrdemServicoDao;
import com.marcelo.backend.apirest.models.services.OrdemServicoService;

@Service
public class OrdemServicoImpl implements OrdemServicoService {

	@Autowired
	IOrdemServicoDao ordemServicoDao;

	@Override
	public List<OrdemServico> findAll() {
		return ordemServicoDao.findAll();
	}

}
