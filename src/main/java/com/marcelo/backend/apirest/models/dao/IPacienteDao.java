package com.marcelo.backend.apirest.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.marcelo.backend.apirest.entity.Paciente;

public interface IPacienteDao extends JpaRepository<Paciente, Long>{

}
