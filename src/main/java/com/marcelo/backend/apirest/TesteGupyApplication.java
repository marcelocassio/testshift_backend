package com.marcelo.backend.apirest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TesteGupyApplication {

	public static void main(String[] args) {
		SpringApplication.run(TesteGupyApplication.class, args);
	}

}
