package com.marcelo.backend.apirest.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.marcelo.backend.apirest.entity.OrdemServicoExame;
import com.marcelo.backend.apirest.models.services.OrdemServicoExameService;
import com.marcelo.backend.apirest.models.services.PacienteService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/api")
public class OrdemServicoController {

	@Autowired
	OrdemServicoExameService ordemServicoExameService;

	@Autowired
	PacienteService pacienteService;

	/**
	 * Enpoint para persistir o ordel de exame no banco de dados
	 * 
	 * @param ordemServico
	 * @param result
	 * @return
	 */
	@ApiOperation(value = "Enpoint para persistir o ordel de exame no banco de dados", response = OrdemServicoExame.class)
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Os dados foram persistidos com exito!"),
			@ApiResponse(code = 400, message = "Os dados sao invalidos para serem persistidos"),
			@ApiResponse(code = 500, message = "Houve um erro no servidor") })
	@PostMapping("/ordemServicoExame")
	public ResponseEntity<?> createServiceOrder(@Valid @RequestBody OrdemServicoExame ordemServico,
			BindingResult result) {

		OrdemServicoExame ordemServicoExameNew = null;
		Map<String, Object> response = new HashMap<>();

		if (result.hasErrors()) {
			List<String> errors = new ArrayList<>();
			result.getFieldErrors().forEach(er -> {
				errors.add(er.getDefaultMessage());
			});
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		try {
			ordemServicoExameNew = ordemServicoExameService.save(ordemServico);
		} catch (Exception e) {
			response.put("message", "Houve um erro ao persistir a ordem de servico");
			response.put("error", e.getMessage().concat(": ").concat(e.getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("message", "A Ordem de servico foi criada satisfatóriamente!");
		response.put("Ordem de servico: ", ordemServicoExameNew);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}

	@GetMapping("/ordemServicoExame")
	public List<OrdemServicoExame> getAll() {
		return ordemServicoExameService.findAll();
	}

	/**
	 * Enpoint para recuperar um arquivo de ordem de servico
	 * 
	 * @param id
	 * @param result
	 * @return
	 */
	@ApiOperation(value = "Enpoint para recuperar um arquivo de ordem de servicos", response = OrdemServicoExame.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Os dados foram recuperados com exito!"),
			@ApiResponse(code = 404, message = "Dados nao encontrados"),
			@ApiResponse(code = 500, message = "Houve um erro no servidor") })
	@GetMapping("/ordemServicoExame/{id}")
	public ResponseEntity<?> show(@PathVariable Long id) {
		Map<String, Object> response = new HashMap<>();
		OrdemServicoExame ordemServicoExame = null;
		try {
			ordemServicoExame = ordemServicoExameService.findById(id);
		} catch (Exception e) {
			response.put("message", "Houve um erro ao realizar a consulta no  banco de datos!");
			response.put("error", e.getMessage().concat(": ").concat(e.getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if (ordemServicoExame == null) {
			response.put("message", "Houve um erro ao recuperar a ordem de exame com id: "
					.concat(id.toString().concat(" - nao existe no banco de dados")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<OrdemServicoExame>(ordemServicoExame, HttpStatus.OK);
	}

	/**
	 * Enpoint para atualizar a ordem de exame no banco de dados
	 * 
	 * @param ordemServico
	 * @param result
	 * @return
	 */
	@ApiOperation(value = "Enpoint para atualizar a ordem de exame no banco de dados", response = OrdemServicoExame.class)
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Os dados foram atualizados com exito!"),
			@ApiResponse(code = 400, message = "Os dados sao invalidos para serem persistidos"),
			@ApiResponse(code = 500, message = "Houve um erro no servidor") })
	@PutMapping("/ordemServicoExame/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody OrdemServicoExame ordemServico, BindingResult result,
			@PathVariable Long id) {
		OrdemServicoExame ordemServicoExameAtual = ordemServicoExameService.findById(id);
		OrdemServicoExame ordemUpdated = null;
		Map<String, Object> response = new HashMap<>();

		if (result.hasErrors()) {
			List<String> errors = new ArrayList<>();
			result.getFieldErrors().forEach(er -> {
				errors.add(er.getDefaultMessage());
			});
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		if (ordemServicoExameAtual == null) {
			response.put("message", "Houve um erro ao recuperar a ordem de exame com id: "
					.concat(id.toString().concat(" - nao existe no banco de dados")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		try {
			ordemServicoExameAtual.setExame(ordemServico.getExame());
			ordemServicoExameAtual.setOrdemServico(ordemServico.getOrdemServico());
			ordemServicoExameAtual.setPreco(ordemServico.getPreco());

			ordemUpdated = ordemServicoExameService.save(ordemServicoExameAtual);
		} catch (Exception e) {
			response.put("message", "Houve um erro ao atualizar a ordem de servico no banco de dados!");
			response.put("error", e.getMessage().concat(": ").concat(e.getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("message", "O exame foi atualizado com sucesso!");
		response.put("exame", ordemUpdated);

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

	}

	/**
	 * Enpoint para apagar uma ordem de exame
	 * 
	 * @param id
	 * @return
	 */

	@ApiOperation(value = "Enpoint para apagar uma ordem de exame", response = OrdemServicoExame.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Os dados foram apagados com exito!"),
			@ApiResponse(code = 500, message = "Houve um erro no servidor") })
	@DeleteMapping("/ordemServicoExame/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		Map<String, Object> response = new HashMap<>();

		try {
			ordemServicoExameService.delete(id);
		} catch (Exception e) {
			response.put("message", "Houve um erro ao eliminar a ordem de servico no banco de dados!");
			response.put("error", e.getMessage().concat(": ").concat(e.getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("message", "a ordem de servico foi elimnado corretamente!");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

}
