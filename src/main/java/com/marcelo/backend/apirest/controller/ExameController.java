package com.marcelo.backend.apirest.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.marcelo.backend.apirest.entity.Exame;
import com.marcelo.backend.apirest.models.services.ExameService;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/api")
public class ExameController {

	@Autowired
	ExameService exameService;

	@GetMapping("/exame")
	public List<Exame> getExames() {
		return exameService.findAll();
	}

}
