package com.marcelo.backend.apirest.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.marcelo.backend.apirest.entity.Paciente;
import com.marcelo.backend.apirest.models.services.PacienteService;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/api")
public class PacienteController {

	@Autowired
	PacienteService pacienteService;

	@GetMapping("/paciente/{id}")
	public ResponseEntity<?> show(@PathVariable Long id) {
		Map<String, Object> response = new HashMap<>();
		Paciente paciente = null;

		try {
			paciente = pacienteService.findById(id);
		} catch (Exception e) {
			response.put("message", "Houve um erro ao realizar a consulta no  banco de datos!");
			response.put("error", e.getMessage().concat(": ").concat(e.getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if (paciente == null) {
			response.put("message", "Houve um erro ao recuperar o paciente com id: "
					.concat(id.toString().concat(" - nao existe no banco de dados")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<Paciente>(paciente, HttpStatus.OK);
	}

}
