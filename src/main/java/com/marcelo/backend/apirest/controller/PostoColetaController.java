package com.marcelo.backend.apirest.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.marcelo.backend.apirest.entity.PostoColeta;
import com.marcelo.backend.apirest.models.services.PostoColetaService;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/api")
public class PostoColetaController {

	@Autowired
	PostoColetaService postoColetaService;

	@GetMapping("/posto-coleta")
	public List<PostoColeta> getPostoColeta() {
		return postoColetaService.findAll();
	}

}
