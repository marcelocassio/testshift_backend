package com.marcelo.backend.apirest.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.marcelo.backend.apirest.entity.Medico;
import com.marcelo.backend.apirest.models.services.MedicoService;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/api")
public class MedicoController {

	@Autowired
	MedicoService medicoService;

	@GetMapping("/medico")
	public List<Medico> getMedicos() {
		return medicoService.findAll();
	}

}
