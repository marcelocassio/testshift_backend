package com.marcelo.backend.apirest.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * Modelo de persistencia para as ordens de servico
 * 
 * @author marce
 *
 */

@Entity
@Table(name = "ordem_servico")
public class OrdemServico implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
 
	@Temporal(TemporalType.DATE)
	private Date data;
	
	@OneToOne
	@JoinColumn(name = "paciente_id", referencedColumnName = "id")
	private Paciente paciente;
	private String convenio;
	
	@OneToOne
	@JoinColumn(name = "posto_coleta_id", referencedColumnName = "id")
	private PostoColeta postoColeta;
	
	@OneToOne
	@JoinColumn(name = "medico_id", referencedColumnName = "id")
	private Medico medico;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public String getConvenio() {
		return convenio;
	}

	public void setConvenio(String convenio) {
		this.convenio = convenio;
	}

	public PostoColeta getPostoColeta() {
		return postoColeta;
	}

	public void setPostoColeta(PostoColeta postoColeta) {
		this.postoColeta = postoColeta;
	}

	public Medico getMedico() {
		return medico;
	}

	public void setMedico(Medico medico) {
		this.medico = medico;
	}

	/**
	 * Identificador do serializable
	 */
	private static final long serialVersionUID = 1L;

}
