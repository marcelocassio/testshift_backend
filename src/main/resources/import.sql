/* Populate data  Paciente*/
INSERT INTO paciente (id, data_nascimento, endereco, nome, sexo) VALUES(1, '1985-01-01', 'Rua Parana 2551','André Oliveira', 'Masculino');
INSERT INTO paciente (id, data_nascimento, endereco, nome, sexo) VALUES(2, '1986-06-03', 'Av. Junqueira 5241','Maria Santos', 'Feminino');
INSERT INTO paciente (id, data_nascimento, endereco, nome, sexo) VALUES(3, '1988-05-08', 'Av. Brasilia 125','Joao Silva', 'Masculino');
INSERT INTO paciente (id, data_nascimento, endereco, nome, sexo) VALUES(4, '1992-12-15', 'Av. Argentina 9844','Fernando Vasconcelos', 'Masculino');
INSERT INTO paciente (id, data_nascimento, endereco, nome, sexo) VALUES(5, '1993-07-13', 'Rua. Calegari 154','Gilberto Gil', 'Masculino');
INSERT INTO paciente (id, data_nascimento, endereco, nome, sexo) VALUES(6, '1981-09-26', 'Av. Pontape 784','Andressa Zarth', 'Feminino');

/* Populate data Examen */
INSERT INTO exame (id, descricao, preco) VALUES (1, 'Hemograma', 300.00);
INSERT INTO exame (id, descricao, preco) VALUES (2, 'Glicose', 250.00);
INSERT INTO exame (id, descricao, preco) VALUES (3, 'Urina', 150.00);
INSERT INTO exame (id, descricao, preco) VALUES (4, 'Fezes', 200.00);
INSERT INTO exame (id, descricao, preco) VALUES (5, 'Ácido úrico', 180);
INSERT INTO exame (id, descricao, preco) VALUES (6, 'Albumina', 220);

/* Populate data Medico */
INSERT INTO medico (id, especialidade, nome) VALUES (1, 'Clinica Médica','Renan Da Silva');
INSERT INTO medico (id, especialidade, nome) VALUES (2, 'Hematologia','Roberto Vilao');
INSERT INTO medico (id, especialidade, nome) VALUES (3, 'Infectologia','Andressa Magalhaes');
INSERT INTO medico (id, especialidade, nome) VALUES (4, 'Genética','Pedro Constantini');

INSERT INTO posto_coleta (id, descricao, endereco) VALUES (1, 'Posto 001', 'Rua Cerejeiras 2426');
INSERT INTO posto_coleta (id, descricao, endereco) VALUES (2, 'Posto 002', 'Rua Salomao 970');
INSERT INTO posto_coleta (id, descricao, endereco) VALUES (3, 'Posto 003', 'Rua Joao Melo 2074');
